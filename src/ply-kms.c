/* ply-kms.c - KMS routines
 *
 * Copyright (C) 2012, The Chromium OS Authors.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#include <stddef.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

#include "ply-utils.h"

int ply_kms_open() {
  const char *kModuleList[] = { "i915", "exynos", "cirrus", "rockchip", "msm" };
  int fd = -1;
  int i;

  for (i = 0; i < ARRAY_SIZE(kModuleList); i++) {
    fd = drmOpen(kModuleList[i], NULL);
    if (fd >= 0)
      break;
  }

  return fd;
}

