/* ply-gamma.c - plymouth gamma setup via KMS
 *
 * Copyright (C) 2011, The Chromium OS Authors.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef PLY_GAMMA_H
#define PLY_GAMMA_H

#include <stdbool.h>

#include "ply-utils.h"

/* Set the gamma of the screen. */
bool ply_gamma_set(const char* file);

#endif /* PLY_GAMMA_H */
