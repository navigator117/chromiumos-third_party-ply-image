/* ply-frame-buffer.h - framebuffer abstraction
 *
 * Copyright (C) 2007 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * Written By: Ray Strode <rstrode@redhat.com>
 */
#ifndef PLY_FRAME_BUFFER_H
#define PLY_FRAME_BUFFER_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct _ply_frame_buffer_area ply_frame_buffer_area_t;
typedef struct _ply_frame_buffer ply_frame_buffer_t;
typedef struct _ply_list ply_list_t;

struct _ply_frame_buffer_area {
  long x;
  long y;
  size_t width;
  size_t height;
  size_t visible_width;
  size_t visible_height;
};

struct _ply_frame_buffer {
  char *device_name;
  int   device_fd;
  int   drm_device_fd;

  char *map_address;
  size_t size;

  uint32_t *shadow_buffer;

  uint32_t red_bit_position;
  uint32_t green_bit_position;
  uint32_t blue_bit_position;
  uint32_t alpha_bit_position;

  uint32_t bits_for_red;
  uint32_t bits_for_green;
  uint32_t bits_for_blue;
  uint32_t bits_for_alpha;

  int32_t dither_red;
  int32_t dither_green;
  int32_t dither_blue;

  unsigned int bytes_per_pixel;
  unsigned int row_stride;

  ply_frame_buffer_area_t area;
  ply_list_t *areas_to_flush;

  void (*flush_area)(ply_frame_buffer_t      *buffer,
                     ply_frame_buffer_area_t *area_to_flush);

  int pause_count;
};

ply_frame_buffer_t *ply_frame_buffer_new(const char *device_name);
void ply_frame_buffer_free(ply_frame_buffer_t *buffer);

bool ply_frame_buffer_open(ply_frame_buffer_t *buffer);
void ply_frame_buffer_close(ply_frame_buffer_t *buffer);

bool ply_frame_buffer_device_is_open(ply_frame_buffer_t *buffer);
char *ply_frame_buffer_get_device_name(ply_frame_buffer_t *buffer);
void ply_frame_buffer_set_device_name(
    ply_frame_buffer_t *buffer, const char *device_name);
void ply_frame_buffer_get_size(
    ply_frame_buffer_t *buffer, ply_frame_buffer_area_t *size);
const char *ply_frame_buffer_get_bytes(ply_frame_buffer_t *buffer);

bool ply_frame_buffer_fill(ply_frame_buffer_t *buffer,
                           ply_frame_buffer_area_t *area,
                           const uint32_t *data);
void ply_frame_buffer_clear(ply_frame_buffer_t *buffer,
                            uint32_t clear_color);

#endif /* PLY_FRAME_BUFFER_H */
/* vim: set ts=4 sw=4 expandtab autoindent cindent cino={.5s,(0: */
