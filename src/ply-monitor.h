/* ply-monitor.h - monitor setup via KMS
 *
 * Copyright (C) 2012, The Chromium OS Authors.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 */

#ifndef PLY_MONITOR_H
#define PLY_MONITOR_H

#include <stdbool.h>

/* Set this flag if ply_monitor_setup should disable non-main outputs. */
extern bool ply_monitor_set_monitors;

/*
 * Configures the monitors using KMS. This function will find the main monitor
 * (by looking for a connected output, by order of priority: LVDS, eDP,
 * any other), and then will turn off all the other monitors.
 */
bool ply_monitor_setup(ply_frame_buffer_t *buffer);
void ply_monitor_close(ply_frame_buffer_t *buffer);

#endif /* PLY_MONITOR_H */

